var express = require ('express');
var app = express();
var port = 9064;
app.listen(port, console.log("Hello world Docker! Server in Docker is running"));
app.get('/docker', function (req,res) { res.json("Hello World Docker Guys! v2")});
app.get('/docker/rest', function (req,res) { res.json("You wanna a rest service?")});
app.get('/docker/rest/api', function (req,res) {res.json("API webpage in docker container: very soon :)")});

